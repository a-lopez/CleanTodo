﻿using CleanTodo.Domain.Entities;
using CleanTodo.Domain.Interfaces;

namespace CleanTodo.Domain.Services;
public sealed class TodoService
{
    private readonly ITodoWriteRepository _todoWriteRepository;

    public TodoService(ITodoWriteRepository todoWriteRepository)
    {
        _todoWriteRepository = todoWriteRepository;
    }

    public void AddTodoList(string todoListName)
    {
        var todoList = new TodoList(Guid.NewGuid(), todoListName);

        _todoWriteRepository.Insert(todoList);
    }
}
