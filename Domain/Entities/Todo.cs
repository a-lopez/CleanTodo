﻿using CleanTodo.Domain.Primitives;

namespace CleanTodo.Domain.Entities;

public sealed class Todo : Entity
{
    internal Todo(Guid id, string name) : base(id)
    {
        Name = name;
    }

    public string Name { get; private set; }
    public bool IsChecked { get; private set; }

    public void Check(bool isChecked)
    {
        IsChecked = isChecked;
    }
}
