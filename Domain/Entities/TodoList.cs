﻿using CleanTodo.Domain.Events;
using CleanTodo.Domain.Exceptions;
using CleanTodo.Domain.Primitives;

namespace CleanTodo.Domain.Entities;

public sealed class TodoList : Entity
{
    private readonly List<Todo> _todoItems = new();

    public TodoList(Guid id, string name) : base(id)
    {
        Name = name;
    }

    public string Name { get; private set; }

    public Todo CreateTodoItem(string todoItemName)
    {
        if (_todoItems.FirstOrDefault(x => x.Name == todoItemName) is not null)
        {
            throw new TodoItemNameAlreadyExistsException(todoItemName);
        }
        
        var todoItem = new Todo(Guid.NewGuid(), todoItemName);

        _todoItems.Add(todoItem);
        var todoItemEvent = new TodoItemAddedEvent(todoItem);
        base.RegisterDomainEvent(todoItemEvent);

        return todoItem;
    }

    public void CheckTodoItem(Guid todoItemId)
    {
        var todoItem = _todoItems.FirstOrDefault(x => x.Id == todoItemId);
        if (todoItem is null)
        {
            throw new TodoItemNotFoundException(todoItemId);
        }

        todoItem.Check(!todoItem.IsChecked);

        var todoItemCompletedEvent = new TodoItemCompletedEvent(todoItem);
        base.RegisterDomainEvent(todoItemCompletedEvent);
    }
}
