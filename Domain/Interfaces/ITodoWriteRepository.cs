﻿using CleanTodo.Domain.Entities;

namespace CleanTodo.Domain.Interfaces;
public interface ITodoWriteRepository
{
    void Insert(TodoList todoList);
    void Insert(Todo todo);
}
