﻿using CleanTodo.Domain.Entities;

namespace CleanTodo.Domain.Interfaces
{
    public interface ITodoRepository
    {
        Todo Get(string todoListName);
        Todo GetById(Guid id);
    }
}