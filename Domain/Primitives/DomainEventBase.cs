﻿using MediatR;

namespace CleanTodo.Domain.Primitives;

public abstract class DomainEventBase : INotification
{
    public DateTime DateOccurred { get; protected set; } = DateTime.UtcNow;
}
