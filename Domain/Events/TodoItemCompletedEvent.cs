﻿using CleanTodo.Domain.Entities;
using CleanTodo.Domain.Primitives;

namespace CleanTodo.Domain.Events;
internal class TodoItemCompletedEvent : DomainEventBase
{
    public Todo TodoItem { get; set; }
    public TodoItemCompletedEvent(Todo todoItem)
    {
        TodoItem = todoItem;
    }
}
