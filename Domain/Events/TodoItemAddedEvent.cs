﻿using CleanTodo.Domain.Entities;
using CleanTodo.Domain.Primitives;

namespace CleanTodo.Domain.Events;
internal class TodoItemAddedEvent : DomainEventBase
{
    public Todo TodoItem { get; set; }
    public TodoItemAddedEvent(Todo todoItem)
    {
        TodoItem = todoItem;
    }
}
