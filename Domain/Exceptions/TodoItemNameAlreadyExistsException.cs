﻿namespace CleanTodo.Domain.Exceptions;
public sealed class TodoItemNameAlreadyExistsException : ArgumentException
{
    public TodoItemNameAlreadyExistsException(string todoItemName)
        : base($"TodoItem with Name {todoItemName} already exists.")
    {

    }
}
