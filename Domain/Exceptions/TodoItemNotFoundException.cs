﻿namespace CleanTodo.Domain.Exceptions;
public sealed class TodoItemNotFoundException : ArgumentNullException
{
    public TodoItemNotFoundException(Guid todoItemId)
        : base($"The TodoItem with the identifier {todoItemId} was not found.")
    {

    }
}
