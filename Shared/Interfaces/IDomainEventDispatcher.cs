﻿using CleanTodo.Domain.Primitives;

namespace CleanTodo.Shared.Interfaces;
public interface IDomainEventDispatcher
{
    Task DispatchAndClearEvents(IEnumerable<Entity> entitiesWithEvents);
}
