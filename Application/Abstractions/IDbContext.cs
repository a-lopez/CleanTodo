﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanTodo.Application.Abstractions;
internal interface IDbContext : IDisposable
{
    IDbSet<TEntity> Set<TEntity> where TEntity : class;
}
