﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CleanTodo.Application.Todos.Queries;
public sealed record GetTodoByIdQuery(Guid todoId) : IQuery<>
{
}
