﻿using CleanTodo.Domain.Entities;
using CleanTodo.Domain.Interfaces;

namespace CleanTodo.Infrastructure.Data.Repositories.Read;
public sealed class TodoWriteRepository : ITodoWriteRepository
{
    private readonly AppDbContext _dbContext;

    public TodoWriteRepository(AppDbContext dbContext) => _dbContext = dbContext;

    public void Get(Todo todo) => _dbContext.Set<Todo>().Add(todo);
    public void Insert(TodoList todoList) => _dbContext.Set<TodoList>().Add(todoList);
    public void Insert(Todo todo) => _dbContext.Set<Todo>().Add(todo);
}
