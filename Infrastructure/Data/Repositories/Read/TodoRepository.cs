﻿using CleanTodo.Domain.Entities;
using CleanTodo.Domain.Interfaces;

namespace CleanTodo.Infrastructure.Data.Repositories.Read;
public sealed class TodoRepository : ITodoRepository
{
    private readonly AppDbContext _dbContext;

    public TodoRepository(AppDbContext dbContext) => _dbContext = dbContext;

    public Todo Get(string todoListName) => _dbContext.Set<Todo>().First(t => t.Name == todoListName);
    public Todo GetById(Guid id) => _dbContext.Set<Todo>().First(t => t.Id == id);
}
